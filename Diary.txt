															*ROTINA DIÁRIA E OBEJTIVO SEMANAL*

														  OBJETIVOS GERAIS DE SEGUNDA A SEXTA
   ⇒ Acordar às 6 horas da manhã para poder caminhar por mais ou menos 2 horas.
   ⇒ As 10 horas da manhã Assistir a primeira Aula do dia de Trainee da IN Junior.
   ⇒ Depois da aula fazer a atividade da aula.
   ⇒ Almoçar e descansar um pouco.
   ⇒ 14 horas participar da segunda aula do dia de Trainee da IN Junior.
   ⇒ Fazer a atividade da aula.
   ⇒ 17 horas é o momento de descanso e lazer.
   ⇒ 18 horas fazer série de exercícios físicos previamente vistos (40 min de exercícios).
   ⇒ 19 horas estudar para a faculdade (seguir o cronograma já preparado) até as 21horas.
   ⇒ 21 horas Jantar.
   ⇒ Depois da janta descançar.
															  	METAS DIÁRIAS
   ⇒ Hoje em algum momento da noite poder ter um tempo de meditação sozinho(para poder organiar as minhas metas e obetivos futuros).
   ⇒ Conhecer melhor os métodos de gerenciamento de tempo, em especial o Trello.
   ⇒ Poder jogar Warzone que não jogo a um tempo.
																	 AMANHÃ
   ⇒ Continuar fazendo as metas que propus para hoje.
   ⇒ Procurar novas alimentaçães para melhorar a dieta.
=========================================================================================
																 METAS PESSOAIS
   ⇒ Aumentar meu peso para 75kg.
   ⇒ Ler 1 livro por mês(atual 0)
   ⇒ Diminuir tempo jogando e procrastinando e aumentar tempo de estudo (tempo de jogo atual 3hrs p/ dia)
   ⇒ Manter foco diário nas matérias da faculdade.
 
																	*DESCRIÇÃO*
Utilizei uma mistura do método GTD com a rotina que já utilizava diariamente, deixando as coisas mais bem detalhadas e mais expostas, gosto de organizar minhas tarefas mesalmente,
posteriormente eu revejo tudo e mudo uma coisa ou outra, mas geralmente minha rotina diária é a mesma.